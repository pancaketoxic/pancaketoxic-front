import React, { useState, useCallback } from 'react'
import styled from 'styled-components'
import { Card, CardBody, Button, Text } from '@toxicfinance/uikit'
import { useWallet } from '@binance-chain/bsc-use-wallet'
import useI18n from 'hooks/useI18n'
import { useAllHarvest } from 'hooks/useHarvest'
import useFarmsWithBalance from 'hooks/useFarmsWithBalance'
import UnlockButton from 'components/UnlockButton'
import ToxicHarvestBalance from './ToxicHarvestBalance'
import ToxicWalletBalance from './ToxicWalletBalance'

const StyledFarmStakingCard = styled(Card)`
  background-size: 256px;
  background-repeat: no-repeat;
  background-position: top right;
  min-height: 376px;
`

const Block = styled.div`
  margin-bottom: 16px;
`

const TokenImageWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
`

const CardImage = styled.img`
  margin-right: 8px;
`

const Label = styled.div`
  color: ${({ theme }) => theme.colors.textSubtle};
  font-size: 18px;
`

const Actions = styled.div`
  margin-top: 24px;
`

const Divider = styled.div`
  background-color: ${({ theme }) => theme.colors.textSubtle};
  height: 1px;
  width: 100%;
  margin: 24px 0 24px 0;
`

const FarmedStakingCard = () => {
  const [pendingTx, setPendingTx] = useState(false)
  const { account } = useWallet()
  const TranslateString = useI18n()
  const farmsWithBalance = useFarmsWithBalance()
  const balancesWithValue = farmsWithBalance.filter((balanceType) => balanceType.balance.toNumber() > 0)

  const { onReward } = useAllHarvest(balancesWithValue.map((farmWithBalance) => farmWithBalance.pid))

  const harvestAllFarms = useCallback(async () => {
    setPendingTx(true)
    try {
      await onReward()
    } catch (error) {
      // TODO: find a way to handle when the user rejects transaction or it fails
    } finally {
      setPendingTx(false)
    }
  }, [onReward])


  return (
    <StyledFarmStakingCard>
      <CardBody>
        <Block>
        <TokenImageWrapper>
        <CardImage
            src="/images/toxic/2.png"
            alt="toxic logo"
            width={64}
            height={64}
          />
          <ToxicHarvestBalance />
          </TokenImageWrapper>
          
          
          <Label>{TranslateString(544, 'TOXICAKE to Harvest')}</Label>
          
        </Block>
        <Divider />
        <Block>
        <TokenImageWrapper>
        <CardImage
            src="/images/toxic/2.png"
            alt="toxic logo"
            width={64}
            height={64}
          />
          <ToxicWalletBalance />
           </TokenImageWrapper>
          <Label>{TranslateString(546, 'TOXICAKE in Wallet')}</Label>
          
        </Block>
        <Actions>
          {account ? (
            <Button id="harvest-all" disabled={balancesWithValue.length <= 0 || pendingTx} onClick={harvestAllFarms}>
              <Text bold color="invertedContrast" fontSize="16px">{pendingTx
                ? TranslateString(548, 'Collecting TOXICAKE')
                : TranslateString(999, `Harvest all (${balancesWithValue.length})`)}</Text>
            </Button>
          ) : (
            <UnlockButton fullWidth />
          )}
        </Actions>
      </CardBody>
    </StyledFarmStakingCard>
  )
}

export default FarmedStakingCard
