import React from 'react'
import styled from 'styled-components'
import { Heading, BaseLayout } from '@toxicfinance/uikit'
import useI18n from 'hooks/useI18n'
import Page from 'components/layout/Page'
import FarmStakingCard from './components/FarmStakingCard'
// import LotteryCard from './components/LotteryCard'
import ToxicStats from './components/ToxicStats'
import TotalValueLockedCard from './components/TotalValueLockedCard'
// import TwitterCard from './components/TwitterCard'

const Hero = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  margin-top: 32px;
  animation: rotate 5s linear infinite;
  animation-duration: 5s;
    animation-timing-function: linear;
    animation-delay: 0s;
    animation-iteration-count: infinite;
    animation-direction: normal;
    animation-fill-mode: none;
    animation-play-state: running;
    animation-name: rotate;
  padding: 32px;
  background: rgb(60, 77, 90);
  border-radius: 8px;
  ${({ theme }) => theme.mediaQueries.lg} {
    padding: 50px;
  }
`

const HeroImage = styled.img`
  width: 37%;
  position: absolute;
  right: 0;
  bottom: -10%;
`
const BuyButton = styled.a`
  margin-top: 20px;
  background-color: ${({ theme }) => theme.colors.primary};
  width: 176px;
  height: 46px;
  border-radius: 4px;
  justify-content: center;
  align-items: center;
  display: flex;
  color: ${({ theme }) => theme.colors.invertedContrast};
  font-weight: bold;
`



const Cards = styled(BaseLayout)`
  align-items: stretch;
  justify-content: stretch;
  margin-bottom: 32px;
  & > div {
    grid-column: span 6;
    width: 100%;
  }
  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }
  ${({ theme }) => theme.mediaQueries.lg} {
    & > div {
      grid-column: span 6;
    }
  }
`

const StakedWrapper = styled.div`
  position: relative;
`

const Home: React.FC = () => {
  const TranslateString = useI18n()

  return (
    <Page>
    <StakedWrapper>
      <Hero>
        <Heading as="h1" size="xl" mb="24px" color="primary">
          {TranslateString(576, 'The Toxic DEFI app on Binance Smart Chain')}
        </Heading>
        <BuyButton href="https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x6DFCBd29f38118418438548B63AfF6066AFda2AA" target="_blank" rel="noreferrer">
            BUY $TOXICAKE
          </BuyButton>
      </Hero>
      <HeroImage src="/images/toxic/2a.png" />
      </StakedWrapper>
      <div>
        <Cards>
          <FarmStakingCard />
          <ToxicStats />
        </Cards>
      </div>
      <TotalValueLockedCard />
      {/* <TwitterCard/> */}
    </Page>
  )
}

export default Home
