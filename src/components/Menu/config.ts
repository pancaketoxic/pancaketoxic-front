import { MenuEntry } from '@toxicfinance/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
        external: true,
      },
      {
        label: 'Liquidity',
        href: 'https://exchange.pancakeswap.finance/#/add/BNB/0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
        external: true,
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Nuclear',
    icon: 'NuclearIcon',
    href: '/nuclear',
  },
 {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PancakeSwap',
        href: 'https://pancakeswap.info/token/0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
        external: true,
      },
    ],
  },
  {
    label: 'Docs',
    icon: 'GitbookIcon',
    href: 'https://docs.pancaketoxic.finance',
    external: true,
  },
  {
    label: 'Audit (Techrate in Progress)',
    icon: 'AuditIcon',
    external: true,
    href: 'https://drive.google.com/',
  },
]

export default config
