import contracts from './contracts'
import { FarmConfig, QuoteToken } from './types'

const farms: FarmConfig[] = [
  
  {
    pid: 2,
    lpSymbol: 'TOXICAKE-BNB LP',
    lpAddresses: {
      97: '',
      56: '0x970a6b221c6137cf0f9490101260dc88cc7b68f6',
    },
    tokenSymbol: 'TOXICAKE',
    tokenAddresses: {
      97: '',
      56: '0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 1,
    lpSymbol: 'TOXICAKE-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0x91b9994537ec27c2812f9407bb09f7b48cd182be',
    },
    tokenSymbol: 'TOXICAKE',
    tokenAddresses: {
      97: '',
      56: '0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 3,
    lpSymbol: 'BNB-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0x1B96B92314C44b159149f7E0303511fB2Fc4774f',
    },
    tokenSymbol: 'BNB',
    tokenAddresses: {
      97: '',
      56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 4,
    lpSymbol: 'BTCB-BNB LP',
    lpAddresses: {
      97: '',
      56: '0x7561eee90e24f3b348e1087a005f78b4c8453524',
    },
    tokenSymbol: 'BTCB',
    tokenAddresses: {
      97: '',
      56: '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 5,
    lpSymbol: 'ETH-BNB LP',
    lpAddresses: {
      97: '',
      56: '0x70d8929d04b60af4fb9b58713ebcf18765ade422',
    },
    tokenSymbol: 'ETH',
    tokenAddresses: {
      97: '',
      56: '0x2170Ed0880ac9A755fd29B2688956BD959F933F8',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  
  // nuclear
  {
    pid: 0,
    isTokenOnly: true,
    lpSymbol: 'TOXICAKE',
    lpAddresses: {
      97: '',
      56: '0x91b9994537ec27c2812f9407bb09f7b48cd182be', // TOXIC-BUSD LP
    },
    tokenSymbol: 'TOXICAKE',
    tokenAddresses: {
      97: '',
      56: '0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
]

export default farms
