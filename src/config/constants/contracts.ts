export default {
  cake: {
    56: '0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
    97: '0x6DFCBd29f38118418438548B63AfF6066AFda2AA',
  },
  masterChef: {
    56: '0x162240BDa2d70dA45873e3299ee7a7bC3F30A549',
    97: '',
  },
  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
  },
  lottery: {
    56: '',
    97: '',
  },
  lotteryNFT: {
    56: '',
    97: '',
  },
  mulltiCall: {
    56: '0x5ddBBc565331db519981EB8287a9F26d7CFca311',
    97: '0x5ddBBc565331db519981EB8287a9F26d7CFca311',
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
  },
}
