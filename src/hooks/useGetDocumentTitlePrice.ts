import { useEffect } from 'react'
import { usePriceToxicBusd } from 'state/hooks'

const useGetDocumentTitlePrice = () => {
  const toxicPriceUsd = usePriceToxicBusd()

  const toxicPriceUsdString = toxicPriceUsd.eq(0)
    ? ''
    : ` - $${toxicPriceUsd.toNumber().toLocaleString(undefined, {
        minimumFractionDigits: 3,
        maximumFractionDigits: 3,
      })}`

  useEffect(() => {
    document.title = `PancakeToxic${toxicPriceUsdString}`
  }, [toxicPriceUsdString])
}
export default useGetDocumentTitlePrice
